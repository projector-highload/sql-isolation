package main

import (
	"context"
	"database/sql"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"

	"golang.org/x/sync/errgroup"
)

var levelTests = []struct {
	Level    sql.IsolationLevel
	Expected float64
}{
	{Level: sql.LevelReadUncommitted, Expected: 5.0},
	{Level: sql.LevelReadCommitted, Expected: 15.0},
	{Level: sql.LevelRepeatableRead, Expected: 15.0},
	{Level: sql.LevelSerializable, Expected: 20.0},
}

func Test_IsolationLevels(t *testing.T) {
	db, err := sql.Open("mysql", "root:qwerty@tcp(127.0.0.1)/projector?parseTime=true")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("SET GLOBAL innodb_status_output=ON")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("SET GLOBAL innodb_status_output_locks=ON")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS balances (id int NOT NULL AUTO_INCREMENT, balance float, PRIMARY KEY (id))")
	if err != nil {
		panic(err)
	}
	db.Close()

	for _, tt := range levelTests {
		t.Run("Testing "+tt.Level.String(), func(t *testing.T) {
			db, err := sql.Open("mysql", "root:qwerty@tcp(127.0.0.1)/projector?parseTime=true")
			if err != nil {
				assert.NoErrorf(t, err, "failed to init db")
			}
			defer db.Close()

			db.SetMaxOpenConns(10)
			db.SetMaxIdleConns(10)

			_, err = db.Exec("DELETE FROM balances")
			assert.NoErrorf(t, err, "failed to truncate db")
			_, err = db.Exec("INSERT INTO balances (balance) VALUES (0)")
			assert.NoErrorf(t, err, "failed to insert balance")
			_, err = db.Exec("COMMIT")
			assert.NoErrorf(t, err, "failed to insert balance")
			_, err = db.Exec("SET autocommit=0")
			assert.NoErrorf(t, err, "failed to insert balance")

			err = modifyBalance(db, tt.Level)
			if !assert.NoErrorf(t, err, "failed to modify balance") {
				t.FailNow()
			}

			var balance float64

			r := db.QueryRow("SELECT balance FROM balances LIMIT 1")
			assert.NoErrorf(t, r.Err(), "failed to fetch balance")
			r.Scan(&balance)

			assert.Equal(t, tt.Expected, balance)
		})
	}
}

func modifyBalance(db *sql.DB, isoLvl sql.IsolationLevel) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	errg := &errgroup.Group{}

	errg.Go(func() error {
		conn, err := db.Conn(ctx)
		if err != nil {
			return err
		}

		// Transaction starts later, but finished earlier.
		if err := addToBalance(ctx, conn, isoLvl, time.Second, 0, 15); err != nil {
			return err
		}

		return nil
	})

	errg.Go(func() error {
		conn, err := db.Conn(ctx)
		if err != nil {
			return err
		}

		// Transaction starts earlier, but finished later.
		if err := addToBalance(ctx, conn, isoLvl, 0, time.Second*2, 5); err != nil {
			return err
		}

		return nil
	})

	return errg.Wait()
}

func addToBalance(ctx context.Context, conn *sql.Conn, isoLvl sql.IsolationLevel, s1, s2 time.Duration, add float64) error {
	time.Sleep(s1)

	tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: isoLvl})
	if err != nil {
		return err
	}

	id := 0
	balance := 0.0

	r := tx.QueryRowContext(ctx, "SELECT * FROM balances LIMIT 1")
	if r.Err() != nil {
		return r.Err()
	}

	if err := r.Scan(&id, &balance); err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.ExecContext(ctx, "UPDATE balances SET balance = ? WHERE id = ?", balance+add, id)
	if err != nil {
		tx.Rollback()
		return err
	}

	time.Sleep(s2)

	return tx.Commit()
}
