package main

import (
	"context"
	"database/sql"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"

	"golang.org/x/sync/errgroup"
)

var levelTests = []struct {
	Level    sql.IsolationLevel
	Expected float64
}{
	{Level: sql.LevelReadUncommitted},
	{Level: sql.LevelReadCommitted},
	{Level: sql.LevelRepeatableRead},
	{Level: sql.LevelSerializable},
}

func Test_IsolationLevels(t *testing.T) {
	db, err := sql.Open("mysql", "root:qwerty@tcp(127.0.0.1)/projector?parseTime=true")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("SET GLOBAL innodb_status_output=ON")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("SET GLOBAL innodb_status_output_locks=ON")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS balances (id int NOT NULL AUTO_INCREMENT, balance float, PRIMARY KEY (id))")
	if err != nil {
		panic(err)
	}
	db.Close()

	for _, tt := range levelTests {
		t.Run("Testing "+tt.Level.String(), func(t *testing.T) {
			db, err := sql.Open("mysql", "root:qwerty@tcp(127.0.0.1)/projector?parseTime=true")
			if err != nil {
				assert.NoErrorf(t, err, "failed to init db")
			}
			defer db.Close()

			db.SetMaxOpenConns(10)
			db.SetMaxIdleConns(10)

			_, err = db.Exec("DELETE FROM balances")
			assert.NoErrorf(t, err, "failed to truncate db")
			_, err = db.Exec("INSERT INTO balances (balance) VALUES (0)")
			assert.NoErrorf(t, err, "failed to insert balance")
			_, err = db.Exec("COMMIT")
			assert.NoErrorf(t, err, "failed to insert balance")
			_, err = db.Exec("SET autocommit=0")
			assert.NoErrorf(t, err, "failed to insert balance")

			err = doTest(t, db, tt.Level)
			if !assert.NoErrorf(t, err, "failed to modify balance") {
				t.FailNow()
			}
		})
	}
}

func doTest(t *testing.T, db *sql.DB, isoLvl sql.IsolationLevel) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	errg := &errgroup.Group{}

	errg.Go(func() error {
		conn, err := db.Conn(ctx)
		if err != nil {
			return err
		}

		tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: isoLvl})
		if err != nil {
			return err
		}

		balanceBefore, err := fetchBalance(ctx, tx)
		if err != nil {
			return err
		}

		time.Sleep(time.Second * 2)

		balanceAfter, err := fetchBalance(ctx, tx)
		if err != nil {
			tx.Rollback()
			return err
		}

		assert.Equal(t, balanceBefore, balanceAfter)

		return tx.Commit()
	})

	errg.Go(func() error {
		conn, err := db.Conn(ctx)
		if err != nil {
			return err
		}

		time.Sleep(time.Second)

		tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: isoLvl})
		if err != nil {
			return err
		}

		if err := updateRows(ctx, tx); err != nil {
			tx.Rollback()
			return err
		}

		return tx.Commit()
	})

	return errg.Wait()
}

func fetchBalance(ctx context.Context, tx *sql.Tx) (float64, error) {
	r := tx.QueryRowContext(ctx, "SELECT balance FROM balances LIMIT 1")
	if r.Err() != nil {
		return 0, r.Err()
	}
	var balance float64
	if err := r.Scan(&balance); err != nil {
		return 0, err
	}

	return balance, nil
}

func updateRows(ctx context.Context, tx *sql.Tx) error {
	_, err := tx.ExecContext(ctx, "UPDATE balances SET balance = balance+100")

	return err
}
