package main

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"

	"golang.org/x/sync/errgroup"
)

var levelTests = []struct {
	Level sql.IsolationLevel
}{
	{Level: sql.LevelReadUncommitted},
	{Level: sql.LevelReadCommitted},
	{Level: sql.LevelRepeatableRead},
	{Level: sql.LevelSerializable},
}

func Test_IsolationLevels(t *testing.T) {
	db, err := sql.Open("mysql", "root:qwerty@tcp(127.0.0.1)/projector?parseTime=true")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("SET GLOBAL innodb_status_output=ON")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("SET GLOBAL innodb_status_output_locks=ON")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS balances (id int NOT NULL AUTO_INCREMENT, balance float, PRIMARY KEY (id))")
	if err != nil {
		panic(err)
	}
	db.Close()

	for _, tt := range levelTests {
		t.Run("Testing "+tt.Level.String(), func(t *testing.T) {
			db, err := sql.Open("mysql", "root:qwerty@tcp(127.0.0.1)/projector?parseTime=true")
			if err != nil {
				assert.NoErrorf(t, err, "failed to init db")
			}
			defer db.Close()

			_, err = db.Exec("DELETE FROM balances")
			assert.NoErrorf(t, err, "failed to truncate db")
			_, err = db.Exec("COMMIT")
			assert.NoErrorf(t, err, "failed to truncate db")
			_, err = db.Exec("INSERT INTO balances (balance) VALUES (0)")
			assert.NoErrorf(t, err, "failed to insert balance")
			_, err = db.Exec("COMMIT")
			assert.NoErrorf(t, err, "failed to insert balance")

			err = modifyBalance(db, tt.Level)
			assert.NoErrorf(t, err, "failed to insert balance")

			var balance float64

			r := db.QueryRow("SELECT balance FROM balances LIMIT 1")
			assert.NoErrorf(t, r.Err(), "failed to fetch balance")
			r.Scan(&balance)

			assert.Equal(t, 20.0, balance)
		})
	}
}

func modifyBalance(db *sql.DB, isoLvl sql.IsolationLevel) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	errg, ctx := errgroup.WithContext(ctx)

	errg.Go(func() error {
		conn, err := db.Conn(ctx)
		if err != nil {
			return err
		}
		defer conn.Close()

		_, err = conn.ExecContext(ctx, "SET autocommit=0")
		if err != nil {
			return err
		}

		if err := addToBalance(ctx, conn, isoLvl, time.Second*3, 15); err != nil {
			return err
		}

		return nil
	})

	errg.Go(func() error {
		conn, err := db.Conn(ctx)
		if err != nil {
			return err
		}
		defer conn.Close()

		_, err = conn.ExecContext(ctx, "SET autocommit=0")
		if err != nil {
			return err
		}

		if err := addToBalance(ctx, conn, isoLvl, time.Second, 5); err != nil {
			return err
		}

		return nil
	})

	return errg.Wait()
}

func addToBalance(ctx context.Context, conn *sql.Conn, isoLvl sql.IsolationLevel, sleep time.Duration, add float64) error {
	tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: isoLvl})
	if err != nil {
		return err
	}

	id := 0
	balance := 0.0

	r := tx.QueryRowContext(ctx, "SELECT * FROM balances LIMIT 1")

	if err := r.Scan(&id, &balance); err != nil {
		tx.Rollback()
		return err
	}

	time.Sleep(sleep)

	_, err = tx.ExecContext(ctx, "UPDATE balances SET balance = ? WHERE id = ?", balance+add, id)
	if err != nil {
		tx.Rollback()

		if mysqlErr, ok := err.(*mysql.MySQLError); ok && mysqlErr.Number == 1213 {
			return addToBalance(ctx, conn, isoLvl, 0, add)
		}

		return err
	}

	return tx.Commit()
}
